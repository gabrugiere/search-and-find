# Search and Find - Mixed Reality

<figure>
  <img src="data/WhatWeWant.png"
       alt="A capture of what we have"
       width="600">
  <figcaption>This is what we wanted. The counter shows a score of 150 points while the remaining time is 1:28. Here, 6 objects are to be found including the red sphere.</figcaption>
</figure>

## 1. The project
The project consists of a "Hidden Objects" game. The aim is as follows:
- During the game, you have severals items that are placed in the room.
- In the lower right-hand corner of your Hololens, you have the object you need to search... and find.
- When you find an object, your score increase.
- The higher your score, the more successful you have been in the game.
- In game, you have a countdown. The game stops when it is at 0.

Operation:
- In app, you have 4 stage.
- First, a person load the room. She looks at the whole room which creates a spatial mapping. This is the "Mapping" mode.
- Secondly, the person choose where she wants place the objects. The object remains in front of her even if she moves, but while landing on a surface as it moves. Whenever she wants, she taps once with her fingers and the object stay put. This is the "Place" mode.
- Thirdly, the first person give the hololens to another person. This new person also taps once with her fingers when she is ready to play and the game start. This is the "Start Game" mode.
- Finally, if the new person find an object, she taps once towards it, the object disappear and the person moves on to the next object. This is the "In game" mode.

<img src="data/schema.png"
	 alt="Play in 4 stages"
	 width="600">

#### 1.1 What we wanted
At the first time, we wanted a lot of different things that would make noise. For exemple, we wanted swords, pens, but also cubes containing objects that are to be found with the sounds. We also wanted that certain objects can move by themselves.

#### 1.2 What we have (Just before the surprise coronavirus attack)
But finally... we have just... three ball that we can put wherever we want and we can search and find in the "In game" mode. (At the same time, if it hadn't been for the surprise coronavirus attack, we probably would have made the best "Hidden Objects" game in the world.)
<figure>
  <img src="data/WhatWeHave.png"
       alt="A capture of what we have"
       width="600">
  <figcaption>"Game Start" is a text that appear during 3s to indicate that the player must play. The blue sphere is a sphere that the player can find. The red sphere is a sphere that stays at the bottom right to indicate the sphere to be searched.</figcaption>
</figure>

## 2. The peripheral device
The app must be used on a version 1 hololens minimum.
<img src="https://d1lss44hh2trtw.cloudfront.net/assets/article/2018/11/05/holobig_feature.jpg" alt="A photo of an hololens" width="600">

## 3. Frameworks and OS
The app is program on Visual Studio on Windows 10 (minimum version: 16299).\
You'll also need the "UrhoSharp" Framework.\
<img src="https://upload.wikimedia.org/wikipedia/commons/6/61/Visual_Studio_2017_logo_and_wordmark.svg" alt="Visual Studio" width="250" style="margin:10px">
<img src="https://upload.wikimedia.org/wikipedia/commons/0/05/Windows_10_Logo.svg" alt="Windows 10" width="250" style="margin:10px">
<img src="https://docs.microsoft.com/en-us/xamarin/graphics-games/urhosharp/introduction-images/urhosharp-icon.png" alt="UrhoSharp" width="50" style="margin:10px">


## 4. What you need to install for the project to compile and run
To compile and run the project, you just need to use the "UrhoSharp.SharpReality" nuget.

## 5. Others
You can find an explicative note with an documentation and explanations [here](explicative_note.md) (explicative_note.md).\
And our feedbacks [here](feedback.md) (feedback.md).

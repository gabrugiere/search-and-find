﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SearchAndFind
{
    /*
     * The ScoreMultiplier class contain all methods to upadate a score multplier in real time
     */
    class ScoreMultiplier
    {
        //The score factor
        private float scoreFactor;
        //the update rate of the score factor
        private float updateFactor;

        public ScoreMultiplier(float initialScoreFactor, float updateFactor)
        {
            this.scoreFactor = scoreFactor;
            this.updateFactor = updateFactor;
        }

        //This methods will be cllaed by the timer, it update the score factor
        public void update(Object stateInfo)
        {
            AutoResetEvent autoEvent = (AutoResetEvent)stateInfo;
            scoreFactor = scoreFactor *updateFactor;
            autoEvent.Set();
        }

        //The acessor of the score factor
        public float getFactor()
        {
            return scoreFactor;
        }
    }
}
